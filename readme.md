# [URL grabber (beta)](https://bitbucket.org/askorpukov/urlgrabber/)
The simple script collecting URLs from commonly used HTML attributes.

Requiers: Python 3.6, lxml, html5lib.
Takes one argument which is URL to a web page or local path to a HTML file:

```sh
$ python urlgrabber.py https://url.to.your/file.html
```

## Installation and launch

#### Using virtual environment

1. Set up virtual environment (for the first time):

    ```
    $ python -m venv venv
    ```

2. Activate virtual environment:

    * Windows

        ```
        $ venv\Scripts\activate.bat
        ```

    * Unix

        ```
        $ venv\Scripts\activate.sh
        ```

3. Upgrade packet manager (for the first time, could be not necessary):

    ```
    $ python -m pip install --upgrade pip
    ```

4. Install requirements:

    ```
    $ pip install -r requirements.txt
    ```

5. Run:

    ```
    $ python urlgrabber.py https://url.to.your/file.html
    ```

6. Exit virtual environment:

    ```
    $ deactivate
    ```

#### Simple
1. Install requirements:

    ```
    $ pip install -r requirements.txt
    ```

2. Run:

    ```
    $ python urlgrabber.py https://url.to.your/file.html
    ```
