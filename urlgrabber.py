from lxml.html import html5parser, tostring
from urllib.error import URLError, HTTPError, ContentTooShortError
from urllib.parse import quote, urlsplit, urlunsplit
import os, sys, logging, hashlib, random, warnings, re
from logging.handlers import MemoryHandler
from datetime import datetime


# convert an IRI to an URI (works incorrect: converts already converted parts of the string)
def iri2uri(iri):
	parts = urlsplit(iri)
	uri = urlunsplit((	parts.scheme, 
						parts.netloc.encode('idna').decode('ascii'), 
						quote(parts.path),
						quote(parts.query, '='),
						quote(parts.fragment)))
	return uri

# load and parse html
def getHTML(url, log):
	log.info('Loading file.')
	try:
		page = html5parser.parse(url)
		log.info('File successfully loaded.')
		return page
	#except UnicodeEncodeError as e:
	#	try:
	#		page = html5parser.parse(iri2uri(url))
	#		log.info('File successfully loaded.')
	#		return page
	except (FileNotFoundError):
		log.error(f'Error getting file: "{url}". File not found.')
		sys.exit()
	except HTTPError as e:
		log.error(f'Error getting URL: "{url}". HTTP {e.code}: {e.reason}.')
		sys.exit()
	except URLError as e:
		log.error(f'Error getting URL: "{url}". {e.reason}.')
		sys.exit()
	except (ContentTooShortError):
		log.error(f'Error getting URL: "{url}". Downloaded data is less than the expected amount.')
		sys.exit()
	except Exception as e:
		log.error(f'Error getting "{url}". Unknown error: {e}.')
		sys.exit()

# generate folder name for results
def generateFolderName(page):
	date = datetime.now().strftime("%Y.%m.%d_%H-%M-%S")
	datahash = hashlib.md5(tostring(page)).hexdigest()
	folder = f'{datahash}_{date}'
	while os.path.isdir(folder):
		folder = folder + str(random.randint(0, 9))
	return folder

# pase internal JavaScript (contents of <script> tag)
def parseInternalScripts(page, xPath, folder, subfolder, msg, log):
	internalScripts = page.xpath(xPath)
	if len(internalScripts) > 0:
		for idx, script in enumerate(internalScripts, start=1):
			filePath = os.path.join(folder, subfolder, f'int{idx}.js_')
			os.makedirs(os.path.dirname(filePath), exist_ok=True)
			with open(filePath, 'w', encoding="utf-8") as out_file:
				out_file.write(script)
				log.info(f'{msg} #{idx} of {len(internalScripts)} saved to "{out_file.name}".')
	else:
		log.info(f'{msg} not found.')

# parse event handler attributes (attributes like 'on[eventname]')
def parseEventHandlerAttributes(page, folder, subfolder, msg, log):
	eventHandlerAttributes = dict.fromkeys(['onabort', 'onautocomplete', 'onautocompleteerror', 'onblur', 'oncancel', 'oncanplay', 'oncanplaythrough', 'onchange', 'onclick', 'onclose', 'oncontextmenu', 'oncuechange', 'ondblclick', 'ondrag', 'ondragend', 'ondragenter', 'ondragexit', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'ondurationchange', 'onemptied', 'onended', 'onerror', 'onfocus', 'oninput', 'oninvalid', 'onkeydown', 'onkeypress', 'onkeyup', 'onload', 'onloadeddata', 'onloadedmetadata', 'onloadstart', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onpause', 'onplay', 'onplaying', 'onprogress', 'onratechange', 'onreset', 'onresize', 'onscroll', 'onseeked', 'onseeking', 'onselect', 'onshow', 'onsort', 'onstalled', 'onsubmit', 'onsuspend', 'ontimeupdate', 'ontoggle', 'onvolumechange', 'onwaiting'])

	for attrName, attrValue in eventHandlerAttributes.items():
		findAttrValue = page.xpath(f"//*/@{attrName}")
		if len(findAttrValue) > 0:
			eventHandlerAttributes[attrName] = findAttrValue
	
	for attrName, attrValue in eventHandlerAttributes.items():
		if attrValue is not None:
			if len(attrValue) > 0:
				for idx, script in enumerate(attrValue, start=1):
					filePath = os.path.join(folder, subfolder, f'{attrName}_{idx}.js_')
					os.makedirs(os.path.dirname(filePath), exist_ok=True)
					with open(filePath, 'w', encoding="utf-8") as out_file:
						out_file.write(script)
						log.info(f'{msg} \'{attrName}\' #{idx} of {len(attrValue)} saved to "{out_file.name}".')
	
	if len(list(filter(None.__ne__, eventHandlerAttributes.values()))) == 0:
		log.info(f'{msg}s not found.')

# parse tags attributes by xPath
# parseTagsAttributes(page, "//*[local-name() = 'tagName']/@attrName", folder, 'filename.txt', 'Short description')
def parseTagsAttributes(page, xPath, folder, file, msg, log):
	attributes = page.xpath(xPath)
	if len(attributes) > 0:
		with open(os.path.join(folder, file), 'w', encoding="utf-8") as out_file:
			out_file.write('\n'.join(attributes))
		log.info(f'{msg} [total: {len(attributes)}] saved to "{out_file.name}".')
	else:
		log.info(f'{msg} not found.')

# parse meta refresh
def parseMetaRefresh(page, xPath, folder, file, msg, log):
	metaTags = page.xpath(xPath)
	URLs = []
	if len(metaTags) > 0:
		for content in metaTags:
			matchURL = re.match(r'(?i)^\s*\d+\s*\;\s*(url\s*=\s*)?(?P<quote>[\"\'\`])?(?!(?P=quote)\s*$)?(.*?)(?P=quote)?\s*$', content)
			if matchURL:
				URLs.append(matchURL.group(3))
	if len(URLs) > 0:
		with open(os.path.join(folder, file), 'w', encoding="utf-8") as out_file:
			out_file.write('\n'.join(URLs))
		log.info(f'{msg} [total: {len(URLs)}] saved to "{out_file.name}".')
	else:
		log.info(f'{msg} not found.')



def main():
	if len(sys.argv) <= 1:
		print('At least one argument is required: URL or path to file.')
		sys.exit() #quit()

	# set up logging to file and console
	logging.basicConfig(level=logging.NOTSET,
						format='[%(levelname)s]\t%(message)s',
						datefmt='%Y.%m.%d %H:%M:%S')
	logger = logging.getLogger(__name__)

	# save logging data to memory before the log file is created
	memhandler = MemoryHandler(1024*100)
	logger.addHandler(memhandler)

	URL = sys.argv[1]
	logger.info(f'Looking for URLs and JavaScripts in file: "{URL}".')

	# 'switsh off' html5lib parser warnings
	warnings.filterwarnings("ignore")
	#with warnings.catch_warnings():
	#	warnings.simplefilter("ignore")
	#	page = getHTML(URL, logger)

	# load file and parse html code
	page = getHTML(URL, logger)

	# generate folder name and create it
	folder = generateFolderName(page)
	os.makedirs(folder, exist_ok=True)
	logger.info(f'Folder created: "{folder}".')

	# create log file and save buffered records
	parselogfile = os.path.join(folder, 'parse.log')
	filehandler = logging.FileHandler(parselogfile)
	filehandler.setLevel(logging.DEBUG)
	filehandler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
	memhandler.setTarget(filehandler)
	memhandler.flush()
	memhandler.close()
	logger.removeHandler(memhandler)
	logger.addHandler(filehandler)
	logger.info(f'Log file created: "{parselogfile}".')


	# parse tags and attributes and save extracted data
	logger.info('Parsing file.')

	# parse script:not([src])
	parseInternalScripts(page, "//*[local-name() = 'script' and not(@src)]/text()", folder, 'internal_js', 'Internal JavaScript', logger)

	# parse script[src]
	parseTagsAttributes(page, "//*[local-name() = 'script']/@src", folder, 'external_js_urls.txt', 'External JavaScript URLs', logger)

	# parse a[href]
	parseTagsAttributes(page, "//*[local-name() = 'a']/@href", folder, 'hyperlinks.txt', 'Hyperlinks URLs', logger)

	# parse iframe[src]
	parseTagsAttributes(page, "//*[local-name() = 'iframe']/@src", folder, 'iframes.txt', 'Iframes source URLs', logger)

	# parse form[action]
	parseTagsAttributes(page, "//*[local-name() = 'form']/@action", folder, 'forms.txt', 'Forms action URLs', logger)

	# parse embed[src]
	parseTagsAttributes(page, "//*[local-name() = 'embed']/@src", folder, 'embeds.txt', 'Embeds source URLs', logger)

	# parse object[data]
	parseTagsAttributes(page, "//*[local-name() = 'object']/@data", folder, 'objects.txt', 'Objects data URLs', logger)

	# parse base[href]
	parseTagsAttributes(page, "//*[local-name() = 'base']/@href", folder, 'bases.txt', 'Base URLs', logger)

	# parse link[href]
	parseTagsAttributes(page, "//*[local-name() = 'link']/@href", folder, 'links.txt', 'Links URLs', logger)

	# parse area[href]
	parseTagsAttributes(page, "//*[local-name() = 'area']/@href", folder, 'areas.txt', 'Areas URLs', logger)

	# parse img[src]
	parseTagsAttributes(page, "//*[local-name() = 'img']/@src", folder, 'imgs.txt', 'Images source URLs', logger)

	# parse input[src]
	parseTagsAttributes(page, "//*[local-name() = 'input']/@src", folder, 'inputs.txt', 'Inputs source (image) URLs', logger)

	# parse event handler attributes
	parseEventHandlerAttributes(page, folder, 'eventhandlers', 'Event handler attribute', logger)

	# detect meta refresh (HTML redirect) and extract URLs
	parseMetaRefresh(page, "//*[local-name() = 'meta' and contains(translate(@http-equiv, 'REFSH', 'refsh'), 'refresh') and number(substring-before(@content, ';'))>=0]/@content", folder, 'meta_refresh.txt', 'Meta refresh URLs', logger)

	logger.info('Parsing done.')

if __name__ == '__main__':
	main()